<?php
/**
 *  {$action_name}.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */
require_once '{$dir_app}/Tb_Controller.php';

Tb_Controller::main('Tb_Controller', '{$action_name}');
?>

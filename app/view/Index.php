<?php
/**
 *  Index.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Index view implementation.
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class Tb_View_Index extends Tb_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
    }
}

?>

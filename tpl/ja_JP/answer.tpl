<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
    <div class="container">
      <form class="form-inline well" method="post" action="index.php">
        <h3>{$app.q.statement}</h3>
	<input type="hidden" name="id" value="{$app.q.id}">
    	<input type="hidden" name="action_answer_commit" value="true">
    	<input type="text" class="input-small" name="statement" placeholder="回答">
    	<button class="btn btn-large btn-primary" type="submit" name="post">回答する</button>
      </form>
    </div>
</body>
</html>

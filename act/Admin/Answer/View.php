<?php
/**
 *  Admin/Answer/View.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  admin_answer_view Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_AdminAnswerView extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
    );

}

/**
 *  admin_answer_view action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_AdminAnswerView extends Tb_ActionClass
{
    /**
     *  preprocess of admin_answer_view Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //コピペ用
        //$admin_mgr = $this->backend->getManager('admin');
        //$user_mgr = $this->backend->getManager('user');
        //$question_mgr = $this->backend->getManager('question');
        //$answer_mgr = $this->backend->getManager('answer');
        //$this->af->get('');

        return null;
    }

    /**
     *  admin_answer_view action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // コピペ用
        //$this->af->setApp('a', $a);
        //$this->af->setAppNe('a', $a);
        $question_id = $this->af->get('question_id');
        $team_id = $this->af->get('team_id');
        $answers = $this->backend->getManager('result')->getAnswers($question_id, $team_id);

        $majorities = array();
        $minorities = array();

        foreach ($answers as $answer) {
            if ($answer['isMajority'] === 1) {
                array_push($majorities, $answer);
            } else {
                array_push($minorities, $answer);
            }
        }
        $this->af->setApp('majorities', $majorities);
        $this->af->setApp('minorities', $minorities);

        return 'admin_answer_view';
    }
}

<?php
require_once('../lib/MyPDO.php');
/**
 *  Tb_ResultManager.php
 *
 *  @author     {ryusuke.chiba}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_ResultManager
 *
 *  @author     {ryusuke.chiba}
 *  @access     public
 *  @package    Tb
 */
class Tb_ResultManager extends Ethna_AppManager
{
    /**
     * getAnswers
     *
     * @param  int   $question_id
     * @param  int   $team_id
     * @return array $answers     array of answer.
     */
    public function getAnswers($question_id, $team_id) {
        $db = new MyPDO();
        $sql = "SELECT * FROM answer WHERE question_id = :question_id AND team_id = :team_id";
        $data = array(
            'question_id' => $question_id,
            'team_id'     => $team_id,
        );
        $stmt = $db->prepare($sql);
        $result = $stmt->execute($data);
        $answers = $stmt->fetchAll();

        $user_ids = array();
        foreach ($answers as $answer) {
            array_push($user_ids, $answer['user_id']);
        }

        $sql = 'SELECT * FROM user WHERE id IN (' . join(', ', $user_ids) . ')';
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array());
        $users = $stmt->fetchAll();

        foreach ($answers as $key => $answer) {
            foreach ($users as $user) {
                if ($answer['user_id'] === $user['id']) {
                    $answers[$key]['user_name'] = $user['name'];
                }
            }
        }

        return $answers;
    }

    /**
     * getPoint
     *
     * @param  int   $question_id
     * @param  array $answers
     * @return int   $point
     */
    public function getPoint($question_id, $answers) {
        $question_mngr = $this->backend->getManager('question');
        $question = $question_mngr->get($question_id);
        $n_majority = $this->_countMajority($answers);
        return $question['lucky_magnification'] * $n_majority;
    }

    /**
     * _countMajority
     *
     * @param  array $answers
     * @return int   $n_majority
     */
    private function _countMajority($answers){
        $n_majority = 0;
        foreach($answers as $_key => $_val) {
            if ($_val['is_majority'] === true) {
                $n_majority++;
            }
        }
        return $n_majority;
    }
}

<?php
require_once('../lib/MyPDO.php');
/**
 *  Tb_TeamManager.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_TeamManager
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class Tb_TeamManager extends Ethna_AppManager
{
    /**
     * チーム情報を取得する
     *
     * @param int $id
     * @return array
     */
    public function get($id) {
        $db = new MyPDO();
        $sql = 'SELECT * FROM team WHERE id = :id';
        $params = array(':id' => $id);
        $stmt = $db->prepare($sql);
        $result = $stmt->execute($params);
        $team = $stmt->fetch();
        return $team;
    }

    /**
     * getAll
     *
     * @param  void
     * @return array $teams
     */
    public function getAll() {
        $db = new MyPDO();
        $sql = 'SELECT * FROM team';
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array());
        $teams = $stmt->fetchAll();
        return $teams;
    }
}

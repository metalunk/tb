<?php
/**
 *  {$action_name}.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */
chdir(dirname(__FILE__));
require_once '{$dir_app}/Tb_Controller.php';

ini_set('max_execution_time', 0);

Tb_Controller::main_CLI('Tb_Controller', '{$action_name}');
?>

<?php
/**
 *  Answer/Commit.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  answer_commit Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_AnswerCommit extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
            'id' => array(
                'name' => '問題ID',
                'type' => VAR_TYPE_INT,
		),
            'statement' => array(
                'name' => '回答',
                'type' => VAR_TYPE_STRING,
		),
        /*
            // コピペ用
            'a' => array(
                'name' => '',
                'type' => VAR_TYPE_INT,
                //'type' => VAR_TYPE_STRING,
                //'type' => VAR_TYPE_BOOLEAN,
                //'type' => array(VAR_TYPE_STRING),
                //'required' => false,
                //'form_type' => FORM_TYPE_TEXT,
                //'form_type' => FORM_TYPE_HIDDEN,
                //'form_type' => FORM_TYPE_SELECT,
                //'form_type' => FORM_TYPE_TEXTAREA,
                //'required_error' => 'IDが指定されていません',
                //'type_error' => 'IDが存在しないよ',
            ),
        */
    );

}

/**
 *  answer_commit action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_AnswerCommit extends Tb_ActionClass
{
    /**
     *  preprocess of answer_commit Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        return null;
    }

    /**
     *  answer_commit action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
	$q_id = $this->af->get('id');
	$stmt = $this->af->get('statement');
        $user_mgr = $this->backend->getManager('user');
	$do = $user_mgr->answer($_SESSION['id'], $q_id, $stmt);
	header('Location:./?action_question=true');
    }
}

?>

<?php
/**
 *  Admin/Team/Point.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  admin_team_point Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_AdminTeamPoint extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        /*
            // コピペ用
            'a' => array(
                'name' => '',
                'type' => VAR_TYPE_INT,
                //'type' => VAR_TYPE_STRING,
                //'type' => VAR_TYPE_BOOLEAN,
                //'type' => array(VAR_TYPE_STRING),
                //'required' => false,
                //'form_type' => FORM_TYPE_TEXT,
                //'form_type' => FORM_TYPE_HIDDEN,
                //'form_type' => FORM_TYPE_SELECT,
                //'form_type' => FORM_TYPE_TEXTAREA,
                //'required_error' => 'IDが指定されていません',
                //'type_error' => 'IDが存在しないよ',
            ),
        */
    );

}

/**
 *  admin_team_point action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_AdminTeamPoint extends Tb_ActionClass
{
    /**
     *  preprocess of admin_team_point Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //コピペ用
        //$admin_mgr = $this->backend->getManager('admin');
        //$user_mgr = $this->backend->getManager('user');
        //$question_mgr = $this->backend->getManager('question');
        //$answer_mgr = $this->backend->getManager('answer');
        //$this->af->get('');

        return null;
    }

    /**
     *  admin_team_point action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        $admin_mgr = $this->backend->getManager('admin');
        $ranking = $admin_mgr->getRanking();
        $this->af->setApp('ranking', $ranking);

        return 'admin_team_point';
    }
}

?>

<?php
require_once('../lib/MyPDO.php');
/**
 *  Tb_AdminManager.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_AdminManager
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class Tb_AdminManager extends Ethna_AppManager
{
    const ADMIN_NUMBER = 9999;

    /**
     * login
     *
     * @param int $id
     * @return any
     */
    static function login($id) {
        if ($id !== $this->ADMIN_NUMBER) {
            return array();
        }
        return $this;
    }

    /**
     * getUsers
     *
     * @param void
     * @return array
     */
    public function getUsers() {
        $users = $this->backend->getManager('user')->getAll();
        return $users;
    }

    /**
     * getTeams
     *
     * @return array
     */
    public function getTeams() {
        $teams = $this->backend->getManager('team')->getAll();
        return $teams;
    }

    /**
     * getQuestions
     *
     * @return array
     */
    public function getQuestions() {
        $questions = $this->backend->getManager('question')->getAll();
        return $questions;
    }

    /**
     * getAnswersOfTeam
     *
     * @param int $question_id
     * @param int $team_id
     * @return array
     */
    public function getAnswersOfTeam($question_id, $team_id) {
        $answers = $this->backend->getManager('result')->getAnswers($question_id, $team_id);
        return $answers;
    }

    /**
     * addPoint
     *
     * @param int $question_id
     * @param int $team_id
     */
    public function addPoint($question_id, $team_id) {
        $answers = $this->getAnswersOfTeam($question_id, $team_id);
        $point = $this->backend->getManager('result')->getPoint($question_id, $answers);

        $db = new MyPDO();
        $sql = 'UPDATE team SET sum_point = sum_point + :point WHERE id = :team_id';
        $params = array(
            ':point' => $point,
            ':team_id' => $team_id
        );
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute($params);

        $this->_buildAddPointFlag($team_id, $question_id);
    }

    /**
     * buildAddPointFlag
     *
     * @param  int $team_id
     * @param  int $question_id
     * @return void
     */
    private function _buildAddPointFlag($team_id, $question_id) {
        $db = new MyPDO();
        $sql = "INSERT INTO point_manager (team_id, question_id, status) VALUES (:team_id, :question_id, 1)";
        $stmt = $db->prepare($sql);
        $params = array(
            ':team_id'     => $team_id,
            ':question_id' => $question_id,
        );
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute($params);
    }

    /**
     * getAddPointFlag
     *
     * @param  int $team_id
     * @param  int $question_id
     * @return int status        0 : did not add point
     *                           1 : added point
     */
    public function getAddPointFlag($team_id, $question_id) {
        $db = new MyPDO();
        $sql = 'SELECT * FROM point_manager where team_id = :team_id AND question_id = :question_id';
        $stmt = $db->prepare($sql);
        $params = array(
            ':team_id'     => $team_id,
            ':question_id' => $question_id,
        );
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute($params);
        $res = $stmt->fetchRow();
        return $res['status'];
    }

    /**
     * getRanking
     *
     * @return array
     */
    public function getRanking() {
        $db = new MyPDO();
        $sql = 'SELECT * FROM team ORDER BY sum_point DESC';
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array());
        $ranking = $stmt->fetchAll();
        return $ranking;
    }
}

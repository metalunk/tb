<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
<div class="container">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>チーム</th>
            <th>マジョリティ</th>
            <th>マイノリティ</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{$app.team_id}</td>
            <td>
                {foreach from=$app.majorities item=majority}
                    {$majority.statement}({$majority.user_name})<br />
                {/foreach}
            </td>
            <td>
                {foreach from=$app.minorities item=minority}
                    {$minority.statement}({$minority.user_name})<br />
                {/foreach}
            </td>
          </tr>
        </tbody>
      </table>
</div>
</body>
</html>

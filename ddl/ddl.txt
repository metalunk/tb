# user table
DROP TABLE IF EXISTS user;
CREATE TABLE IF NOT EXISTS user(
    id int unsigned NOT NULL,
    name varchar(50) NOT NULL,
    team_id int unsigned,
    ctime datetime DEFAULT NULL,
    mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# team table
DROP TABLE IF EXISTS team;
CREATE TABLE IF NOT EXISTS team(
    id int unsigned NOT NULL,
    name varchar(50) NOT NULL,
    sum_point int DEFAULT 0,
    ctime datetime DEFAULT NULL,
    mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# question table
DROP TABLE IF EXISTS question;
CREATE TABLE IF NOT EXISTS question(
    id int unsigned NOT NULL AUTO_INCREMENT,
    sequence int NOT NULL,
    status int NOT NULL,
    statement text NOT NULL,
    lucky_magnification int NOT NULL DEFAULT 1,
    ctime datetime DEFAULT NULL,
    mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# point_manager table
DROP TABLE IF EXISTS point_manager;
CREATE TABLE IF NOT EXISTS point_manager(
    id int unsigned NOT NULL AUTO_INCREMENT,
    team_id int unsigned NOT NULL,
    question_id int unsigned NOT NULL,
    status int DEFAULT 0,
    ctime datetime DEFAULT NULL,
    mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# answer table
DROP TABLE IF EXISTS answer;
CREATE TABLE IF NOT EXISTS answer(
    id int unsigned NOT NULL AUTO_INCREMENT,
    user_id int unsigned NOT NULL,
    question_id int unsigned NOT NULL,
    statement text,
    is_majority bool NOT NULL DEFAULT FALSE,
    ctime datetime DEFAULT NULL,
    mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    INDEX user_question (user_id, question_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

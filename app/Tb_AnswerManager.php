<?php
/**
 *  Tb_AnswerManager.php
 *
 *  @author     {ryusuke.chiba}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_AnswerManager
 *
 *  @author     {ryusuke.chiba}
 *  @access     public
 *  @package    Tb
 */
class Tb_AnswerManager extends Ethna_AppManager
{
    /**
     * get
     *
     * @param  int $id
     * @return array
     */
    public function get($id) {
        $sql = "SELECT * FROM answer WHERE id = :id";
        $data = array('id' => $id);
        $stmt = $this->db->db->prepare($sql, $data);
        $result = $this->db->db->execute($stmt);
        $answer = $result->fetchRow();
        return $answer;
    }

    /**
     * getAll
     *
     * @param  void
     * @return array
     */
    public function getAll() {
        $sql = "SELECT * FROM answer";
        $stmt = $this->db->db->prepare($sql);
        $result = $this->db->db->execute($stmt);
        $answers = $result->fetchAll();
        return $answers;
    }
}

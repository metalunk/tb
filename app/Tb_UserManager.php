<?php
require_once('../lib/MyPDO.php');
/**
 *  Tb_UserManager.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_UserManager
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class Tb_UserManager extends Ethna_AppManager
{
    /**
     * login
     *
     * @param int $id
     * @return array
     */
    static function login($id) {
        $user = $this->get($id);
        return $user;
    }

    /**
     * get
     *
     * @param int $id
     * @return array
     */
    public function get($id) {
        $sql = 'SELECT * FROM user WHERE id = :id';
        $db = new MyPDO();
        $params = array(':id' => $id);
        $stmt = $db->prepare($sql);
        $result = $stmt->execute($params);
        $user = $stmt->fetch();
        return $user;
    }

    /**
     * getAll
     *
     * @param  void
     * @return array
     */
    public function getAll() {
        $sql = 'SELECT * FROM user';
        $stmt = $this->db->db->prepare($sql);
        $result = $this->db->db->execute($stmt);
        $users = $result->fetchAll();
        return $users;
    }

    //TODO: validation not to answer twice
    //    : add Exception when insert failed
    /**
     * answer
     *
     * @param int $user_id
     * @param int $question_id
     * @param string $statement
     * @return void
     */
    public function answer($user_id, $question_id, $statement) {
        $sql = 'INSERT INTO answer (user_id, question_id, statement, ctime)
            VALUES (:user_id, :question_id, :statement, NOW())';
        $db = new MyPDO();
        $params[':user_id'] = $user_id;
        $params[':question_id'] = $question_id;
        $params[':statement'] = $statement;
        $stmt = $db->prepare($sql);
        $result = $stmt->execute($params);
    }

    /**
     * getQuestion
     *
     * @param int $question_id
     * @return array
     */
    public function getQuestion($question_id) {
        $sql = 'SELECT * FROM question WHERE id = :id';
        $data = array('id' => $question_id);
        $stmt = $this->db->db->prepare($sql, $data);
        $result = $this->db->db->execute($stmt);
        $question = $result->fetchRow();
        return $question;
    }

    /**
     * getAllQuestion
     *
     * @return array
     */
    public function getAllQuestion() {
        $sql = 'SELECT * FROM question';
        $stmt = $this->db->db->prepare($sql);
        $result = $this->db->db->execute($stmt);
        $questions = $result->fetchAll();
        return $questions;
    }
}

<?php
/**
 *  Login/Commit.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  login_commit Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_LoginCommit extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'id' => VAR_TYPE_INT
    );

}

/**
 *  login_commit action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_LoginCommit extends Tb_ActionClass
{
    private $user;

    /**
     *  preprocess of login_commit Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $user_mgr = $this->backend->getManager('user');
        $user_id = $this->af->get('id');
        $this->user = $user_mgr->get($user_id);

        // $team_mgr = $this->backend->getManager('team');
        // $team = $team_mgr->get($this->user['team_id']);
        // $this->user['team_name'] = $team['name'];
        if (!$this->user) {
            return 'login';
        }

        $team_mgr = $this->backend->getManager('team');
        $team = $team_mgr->get($this->user['team_id']);
        $this->user['team_name'] = $team['name'];

        return null;
    }

    /**
     *  login_commit action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
	$this->session->start();
        $_SESSION['id'] = $this->user['id'];
        $_SESSION['name'] = $this->user['name'];
        $_SESSION['team_id'] = $this->user['team_id'];
        // $_SESSION['team_name'] = $this->user['team_name'];
        header('location: ?action_question=true');
    }
}

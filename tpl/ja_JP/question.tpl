<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
<div class="container">
      <h4>クイズ一覧</h4>
      <div>出題中の問題にのみ入れます</div>
      <div>いってらっしゃい！</div>
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>No.</th>
          </tr>
        </thead>
        <tbody>
          {foreach from=$app.questions key=k item=q}
          <tr>
            <td>
              {if in_array($q.id, $app.open_q_ids)}
                <a href="?action_answer=true&id={$q.id}">{$q.sequence}</a>
              {else}
                {$q.sequence}
              {/if}
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
</div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
<div class="container">
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>社員番号</th>
            <th>名前</th>
            <th>グループ</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1111</td>
            <td>俺</td>
            <td>1</td>
          </tr>
          <tr>
            <td>1234</td>
            <td>あなた</td>
            <td>2</td>
          </tr>
        </tbody>
      </table>
</div>
</body>
</html>

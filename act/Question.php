<?php
/**
 *  Question.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  question Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_Question extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        /*
            // コピペ用
            'a' => array(
                'name' => '',
                'type' => VAR_TYPE_INT,
                //'type' => VAR_TYPE_STRING,
                //'type' => VAR_TYPE_BOOLEAN,
                //'type' => array(VAR_TYPE_STRING),
                //'required' => false,
                //'form_type' => FORM_TYPE_TEXT,
                //'form_type' => FORM_TYPE_HIDDEN,
                //'form_type' => FORM_TYPE_SELECT,
                //'form_type' => FORM_TYPE_TEXTAREA,
                //'required_error' => 'IDが指定されていません',
                //'type_error' => 'IDが存在しないよ',
            ),
        */
    );

}

/**
 *  question action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_Question extends Tb_ActionClass
{
    /**
     *  preprocess of question Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        return null;
    }

    /**
     *  question action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        $question_mgr = $this->backend->getManager('question');
        $open_questions = $question_mgr->getOpenQuestions();
        $open_q_ids = array();
        foreach ($open_questions as $_key => $_val) {
            $open_q_ids[] = $_val['id'];
        }

        $this->af->setApp('questions', $question_mgr->getAll());
        $this->af->setApp('open_q_ids', $open_q_ids);

        return 'question';
    }
}

?>

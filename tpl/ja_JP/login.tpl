<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
    <div class="container">
      <form class="form-inline well" method="post" action="index.php">
    	<input type="hidden" name="action_login_commit" value="true">
    	<div style="margin-bottom:15px"><input type="text" class="input-small" name="id" placeholder="社員番号"></div>
    	<button class="btn btn-large btn-primary" type="submit" name="login">ログイン</button>
      </form>
    </div>
</body>
</html>

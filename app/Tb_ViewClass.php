<?php
// vim: foldmethod=marker
/**
 *  Tb_ViewClass.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

// {{{ Tb_ViewClass
/**
 *  View class.
 *
 *  @author     {$author}
 *  @package    Tb
 *  @access     public
 */
class Tb_ViewClass extends Ethna_ViewClass
{
    /**
     *  set common default value.
     *
     *  @access protected
     *  @param  object  Tb_Renderer  Renderer object.
     */
    function _setDefault(&$renderer)
    {
    }

}
// }}}

?>

<?php
/**
 *  Admin/Answer/Detail.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  admin_answer_detail Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_AdminAnswerDetail extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        /*
            // コピペ用
            'a' => array(
                'name' => '',
                'type' => VAR_TYPE_INT,
                //'type' => VAR_TYPE_STRING,
                //'type' => VAR_TYPE_BOOLEAN,
                //'type' => array(VAR_TYPE_STRING),
                //'required' => false,
                //'form_type' => FORM_TYPE_TEXT,
                //'form_type' => FORM_TYPE_HIDDEN,
                //'form_type' => FORM_TYPE_SELECT,
                //'form_type' => FORM_TYPE_TEXTAREA,
                //'required_error' => 'IDが指定されていません',
                //'type_error' => 'IDが存在しないよ',
            ),
        */
    );

}

/**
 *  admin_answer_detail action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_AdminAnswerDetail extends Tb_ActionClass
{
    /**
     *  preprocess of admin_answer_detail Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //コピペ用
        //$admin_mgr = $this->backend->getManager('admin');
        //$user_mgr = $this->backend->getManager('user');
        //$question_mgr = $this->backend->getManager('question');
        //$answer_mgr = $this->backend->getManager('answer');
        //$this->af->get('');

        return null;
    }

    /**
     *  admin_answer_detail action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // コピペ用
        //$this->af->setApp('a', $a);
        //$this->af->setAppNe('a', $a);

        return 'admin_answer_detail';
    }
}

?>

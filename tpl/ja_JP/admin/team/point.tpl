<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  {include file='common/header.tpl'}
</head>
<body>
{include file='common/title.tpl'}
<div class="container">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>チーム</th>
            <th>得点</th>
          </tr>
        </thead>
        <tbody>
          {foreach from=$app.ranking key=rank item=team}
          <tr>
            <td>{$team.name}</td>
            <td>{$team.sum_point}</td>
          </tr>
          {/foreach}
        </tbody>
      </table>
</div>
</body>
</html>

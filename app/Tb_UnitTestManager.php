<?php
/**
 *  Tb_UnitTestManager.php
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb Unit Test Manager Class.
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class Tb_UnitTestManager extends Ethna_UnitTestManager
{
    /**
     *  @var    array   General test case definition.
     */
    var $testcase = array(
        /*
         *  TODO: Write general test case definition here.
         *
         *  Example:
         *
         *  'util' => 'app/UtilTest.php',
         */
    );
}
?>

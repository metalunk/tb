<?php
/**
 *  {$view_path}
 *
 *  @author     {$author}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  {$forward_name} view implementation.
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Tb
 */
class {$view_class} extends Tb_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
    }
}

?>

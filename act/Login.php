<?php
/**
 *  Login.php
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  login Form implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Form_Login extends Tb_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        /*
            // コピペ用
            'a' => array(
                'name' => '',
                'type' => VAR_TYPE_INT,
                //'type' => VAR_TYPE_STRING,
                //'type' => VAR_TYPE_BOOLEAN,
                //'type' => array(VAR_TYPE_STRING),
                //'required' => false,
                //'form_type' => FORM_TYPE_TEXT,
                //'form_type' => FORM_TYPE_HIDDEN,
                //'form_type' => FORM_TYPE_SELECT,
                //'form_type' => FORM_TYPE_TEXTAREA,
                //'required_error' => 'IDが指定されていません',
                //'type_error' => 'IDが存在しないよ',
            ),
        */
    );

}

/**
 *  login action implementation.
 *
 *  @author     ma3tk <masataka0227@gmail.com>
 *  @access     public
 *  @package    Tb
 */
class Tb_Action_Login extends Tb_ActionClass
{
    /**
     *  preprocess of login Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //コピペ用
        //$admin_mgr = $this->backend->getManager('admin');
        //$user_mgr = $this->backend->getManager('user');
        //$question_mgr = $this->backend->getManager('question');
        //$answer_mgr = $this->backend->getManager('answer');
        //$this->af->get('');

        return null;
    }

    /**
     *  login action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // コピペ用
        //$this->af->setApp('a', $a);
        //$this->af->setAppNe('a', $a);

        return 'login';
    }
}

?>

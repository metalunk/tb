<?php
require_once('../lib/MyPDO.php');
/**
 *  Tb_QuestionManager.php
 *
 *  @author     {ryusuke.chiba}
 *  @package    Tb
 *  @version    $Id$
 */

/**
 *  Tb_QuestionManager
 *
 *  @author     {ryusuke.chiba}
 *  @access     public
 *  @package    Tb
 */
class Tb_QuestionManager extends Ethna_AppManager
{
    /**
     * get
     *
     * @param  int $id
     * @return array
     */
    public function get($id) {
        $sql = "SELECT * FROM question WHERE id = :id";
        $db = new MyPDO();
        $params[':id'] = $id;
        $stmt = $db->prepare($sql);
        $result = $stmt->execute($params);
        $question = $stmt->fetch();
        return $question;
    }

    /**
     * getAll
     *
     * @param  void
     * @return array
     */
    public function getAll() {
        $sql = "SELECT * FROM question ORDER BY sequence";
        $db = new MyPDO();
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array());
        $questions = $stmt->fetchAll();
        return $questions;
    }

    public function getOpenQuestions() {
        $sql = "SELECT * FROM question WHERE status = 1";
        $db = new MyPDO();
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array());
        $question = $stmt->fetchAll();
        return $question;
    }
    /**
     * isOpen
     *
     * @param  int  $question_id
     * @return bool
     */
    public function isOpen($question_id) {
        $question = $this->get($question_id);
        if ($question['status']) {
            return true;
        }
        return false;
    }
}
